define(['utils', 'submodule/widget'],
function(utils, widget) {
	'use strict';

	console.log(utils.foo(1, 2));
	console.log(widget.bar(3, 4));
});
