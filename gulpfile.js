var gulp = require('gulp'),
	amdOptimize = require('amd-optimize'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	jshint = require('gulp-jshint'),
	sass = require('gulp-sass');

var target = function(name, src, fn) {
	var buildName = 'build-'+name; target.builds.push(buildName);
	var watchName = 'watch-'+name; target.watches.push(watchName);
	gulp.task(buildName, fn);
	gulp.task(watchName, function() {
		return gulp.watch(src, [buildName]);
	});
};
target.builds = [];
target.watches = [];

/* JAVASCRIPT BUILD TASKS */

var src_js = 'src/**/*.js';
target('js', src_js, function() {
	return gulp.src(src_js)
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
		.pipe(amdOptimize('app'))
		.pipe(concat('app.js'))
		.pipe(uglify())
		.pipe(gulp.dest('build/js'));
});

/* CSS BUILD TASKS */

var src_scss = 'assets/scss/**/*.scss';
target('css', src_scss, function () {
	return gulp.src(src_scss)
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('build/css'));
});


/* COMBINED TASKS */

gulp.task('build', target.builds);
gulp.task('watch', target.watches);
